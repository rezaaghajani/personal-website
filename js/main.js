var typeString = ['Front End Developer', 'Python Developer','Graphic Designer'];
      var  i = 0;
      var count = 0
      var selectedText = '';
      var text = '';
      (function type() {
        if (count == typeString.length) {
          count = 0;
        }
        selectedText = typeString[count];
        text = selectedText.slice(0, ++i);
        document.getElementById('typing').innerHTML = text;
        if (text.length === selectedText.length) {
          count++;
          i = 0;
        }
        setTimeout(type, 200);
      }());

      function sleep(milliseconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
          if ((new Date().getTime() - start) > milliseconds){
            break;
          }
        }
      }


      function domloaded() {
        var c = document.getElementById('canvas');
        var ctx = c.getContext('2d');
        var j = 0, pixSize = 2, pixCount = 50;
        for(var r = 0; r < pixCount; r++) {
             for(var i = 0; i < pixCount; i++) {
                  if(i % pixCount === 0) {
                       j++;
                  }
                  ctx.fillStyle = 'hsl(' + 360 * Math.random() + ', 50%, 50%)';
                  ctx.fillRect(i * pixSize, j * pixSize, 20, 20);
             }
        }
        }
        setInterval(domloaded, 150);


        function preloader() {
          window.onload = function(){ document.getElementById("drawing_container").style.display = "none" }
        }

preloader()



